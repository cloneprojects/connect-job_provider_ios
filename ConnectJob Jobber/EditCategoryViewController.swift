//
//  EditCategoryViewController.swift
//  UberdooXP
//
//  Created by Karthik Sakthivel on 14/11/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner


protocol EditCategoryDelegate: class {
    func didFinishEditingCategories(selectionDone: Bool)
}

class EditCategoryViewController: UIViewController {

    @IBOutlet weak var categoryFld: UITextField!
    @IBOutlet weak var subCategoryFld: UITextField!
    @IBOutlet weak var experienceFld: UITextField!
    @IBOutlet weak var pricePerHourFld: UITextField!
    @IBOutlet weak var quickPitchFld: UITextField!
    
    weak var delegate : EditCategoryDelegate?
    
    var serviceDictionary : [String:JSON]!
    
    @IBOutlet weak var editButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        print(serviceDictionary)
        categoryFld.text = serviceDictionary["category_name"]?.stringValue
        subCategoryFld.text = serviceDictionary["sub_category_name"]?.stringValue
        experienceFld.text = serviceDictionary["experience"]?.stringValue
        pricePerHourFld.text = serviceDictionary["priceperhour"]?.stringValue
        quickPitchFld.text = serviceDictionary["quickpitch"]?.stringValue
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func deleteAction(_ sender: UIButton) {
        deleteCategory()
    }
    @IBAction func editAction(_ sender: UIButton) {
        if(sender.titleLabel?.text == "EDIT"){
            sender.setTitle("SAVE", for: .normal)
            experienceFld.isUserInteractionEnabled = true
            pricePerHourFld.isUserInteractionEnabled = true
            quickPitchFld.isUserInteractionEnabled = true
            
            experienceFld.textColor = UIColor.init(red: 29/255, green: 29/255, blue: 29/255, alpha: 1)
            pricePerHourFld.textColor = UIColor.init(red: 29/255, green: 29/255, blue: 29/255, alpha: 1)
            quickPitchFld.textColor = UIColor.init(red: 29/255, green: 29/255, blue: 29/255, alpha: 1)
        }
        else{
            experienceFld.isUserInteractionEnabled = false
            pricePerHourFld.isUserInteractionEnabled = false
            quickPitchFld.isUserInteractionEnabled = false
            updateCategory()
            sender.setTitle("EDIT", for: .normal)
            
            experienceFld.textColor = UIColor.lightGray
            pricePerHourFld.textColor = UIColor.lightGray
            quickPitchFld.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func updateCategory(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let pricePerHour = self.pricePerHourFld.text!
        let quickpitch = self.quickPitchFld.text!
        let experience = self.experienceFld.text!
        
        let provServiceId = self.serviceDictionary["id"]?.stringValue
        
        let params: Parameters = [
            "provider_service_id": provServiceId!,
            "priceperhour":pricePerHour,
            "quickpitch":quickpitch,
            "experience":experience
        ]
        SwiftSpinner.show("Updating service...")
        let url = "\(Constants.baseURL)/edit_category"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("UPDATE CATEGORY JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        if(self.delegate != nil){                        self.delegate?.didFinishEditingCategories(selectionDone: true)
                            self.dismiss(animated: true, completion: nil);
                        }
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }
    }
    
    
    func deleteCategory(){
        var headers : HTTPHeaders!
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String!
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let provServiceId = self.serviceDictionary["id"]?.stringValue
        
        let params: Parameters = [
            "provider_service_id": provServiceId!
        ]
        SwiftSpinner.show("Removing service...")
        let url = "\(Constants.baseURL)/delete_category"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("DELETE CATEGORY JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SigninViewController") as! SigninViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        if(self.delegate != nil){                        self.delegate?.didFinishEditingCategories(selectionDone: true)
                            self.dismiss(animated: true, completion: nil);
                        }
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }

    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
}
